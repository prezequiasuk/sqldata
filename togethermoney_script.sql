USE [newhopei_test]
GO
/****** Object:  Table [newhopei_santos].[Product]    Script Date: 10/10/2021 18:18:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [newhopei_santos].[Product](
	[ProductId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NULL,
	[ProductDescription] [varchar](200) NULL,
	[Price] [decimal](18, 2) NULL,
	[Sold] [bit] NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[ProductId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [newhopei_santos].[User]    Script Date: 10/10/2021 18:18:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [newhopei_santos].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NULL,
	[Address] [varchar](150) NULL,
	[Postcode] [varchar](20) NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [newhopei_santos].[Product] ON 

INSERT [newhopei_santos].[Product] ([ProductId], [UserId], [ProductDescription], [Price], [Sold]) VALUES (1, 1, N'01 pair of Trainers NIKE', CAST(42.00 AS Decimal(18, 2)), 1)
INSERT [newhopei_santos].[Product] ([ProductId], [UserId], [ProductDescription], [Price], [Sold]) VALUES (2, 2, N'Sport hat', CAST(4.00 AS Decimal(18, 2)), 1)
INSERT [newhopei_santos].[Product] ([ProductId], [UserId], [ProductDescription], [Price], [Sold]) VALUES (3, 2, N'Black shoes', CAST(35.00 AS Decimal(18, 2)), 0)
INSERT [newhopei_santos].[Product] ([ProductId], [UserId], [ProductDescription], [Price], [Sold]) VALUES (4, 1, N'Mac laptop', CAST(1350.00 AS Decimal(18, 2)), 1)
SET IDENTITY_INSERT [newhopei_santos].[Product] OFF
GO
SET IDENTITY_INSERT [newhopei_santos].[User] ON 

INSERT [newhopei_santos].[User] ([UserId], [Username], [Address], [Postcode]) VALUES (1, N'John Davis', N'15 Tatton street, Hulme, Manchester', N'M15 4EQ')
INSERT [newhopei_santos].[User] ([UserId], [Username], [Address], [Postcode]) VALUES (2, N'Mary Robins', N'01 sherwood Avenue, Leyland', N'PR25 5AU')
SET IDENTITY_INSERT [newhopei_santos].[User] OFF
GO
ALTER TABLE [newhopei_santos].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Product] FOREIGN KEY([UserId])
REFERENCES [newhopei_santos].[User] ([UserId])
GO
ALTER TABLE [newhopei_santos].[Product] CHECK CONSTRAINT [FK_Product_Product]
GO
